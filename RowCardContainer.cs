using Godot;
using System;

public partial class RowCardContainer : HFlowContainer
{
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
	public override bool _CanDropData(Vector2 atPosition, Variant data)
    {
        return data.Obj is card;
    }
    public override void _DropData(Vector2 atPosition, Variant data)
    {
        this.GetParentOfType<row>().DropCardOn(atPosition, data);
    }
}
