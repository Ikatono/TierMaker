using Godot;
using System;

public partial class CardCreateImageBox : TextureRect
{
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
	public override void _GuiInput(InputEvent @event)
    {
        if (@event is InputEventMouseButton iemb)
		{
			if (iemb.Pressed && iemb.ButtonIndex == MouseButton.Left)
			{
				GetNode<FileDialog>("%CardImagePicker").Show();
			}
		}
    }
}
