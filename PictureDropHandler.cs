using Godot;
using System;
using System.Collections;
using System.Collections.Generic;

public partial class PictureDropHandler : Node
{
	private Settings _Settings;
	private Settings Settings
	{ get
		{
			_Settings ??= GetNode<Settings>("/root/Settings");
			return _Settings;
		}
	}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GetTree().Root.FilesDropped += HandleDroppedFiles;
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
	public void HandleDroppedFiles(IEnumerable<string> paths)
	{
		//var cont = GetNode<HFlowContainer>("%UnassignedCardContainer");
		var g = GetNode<game>("/root/Game");
		foreach (var path in paths)
		{
			var img = new Image();
			img.Load(path);
			var tex = ImageTexture.CreateFromImage(img);
			if (tex is null)
				continue;
			var c = card.MakeCard(GetTree());
			c.SetImage(tex);
			c.SetStretchMode(Settings.StretchMode);
			g.AddUnassignedCard(c);
		}
	}
}
