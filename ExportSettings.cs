using Godot;

public partial class ExportSettings : GodotObject
{
    /// <summary>
    /// Scales images down to size of the 
    /// </summary>
	public bool ScaleImages { get; set; } = false;
}	