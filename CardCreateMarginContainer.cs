 using Godot;
using System;

public partial class CardCreateMarginContainer : MarginContainer
{
	public readonly string Title = "Card";
	public string CardTitle
	{
		get => GetNode<LineEdit>("%TitleEdit").Text;
		set => GetNode<LineEdit>("%TitleEdit").Text = value;
	}
	public void SetStretchMode(StretchMode stretchMode)
	{
		switch (stretchMode)
		{
			case StretchMode.Unspecified:
			case StretchMode.Fit:
			default:
				GetNode<BaseButton>("%StretchModeFitButton").ButtonPressed = true;
				break;
			case StretchMode.Stretch:
				GetNode<BaseButton>("%StretchModeStretchButton").ButtonPressed = true;
				break;
			case StretchMode.Crop:
				GetNode<BaseButton>("%StretchModeCropButton").ButtonPressed = true;
				break;
		}
	}
	public StretchMode GetStretchMode()
	{
		if (GetNode<BaseButton>("%StretchModeFitButton").ButtonPressed)
			return StretchMode.Fit;
		else if (GetNode<BaseButton>("%StretchModeStretchButton").ButtonPressed)
			return StretchMode.Stretch;
		else if (GetNode<BaseButton>("%StretchModeCropButton").ButtonPressed)
			return StretchMode.Crop;
		return StretchMode.Unspecified;
	}
	private void StetchModeChanged(BaseButton button)
	{
		var ci = GetNode<CardCreateImageBox>("%CardCreateImageBox");
		switch (GetStretchMode())
		{
			case StretchMode.Stretch:
				ci.ExpandMode = TextureRect.ExpandModeEnum.IgnoreSize;
				ci.StretchMode = TextureRect.StretchModeEnum.Scale;
				break;
			case StretchMode.Crop:
				ci.ExpandMode = TextureRect.ExpandModeEnum.IgnoreSize;
				ci.StretchMode = TextureRect.StretchModeEnum.KeepAspectCovered;
				break;
			case StretchMode.Unspecified:
			case StretchMode.Fit:
			default:
				ci.ExpandMode = TextureRect.ExpandModeEnum.IgnoreSize;
				ci.StretchMode = TextureRect.StretchModeEnum.KeepAspectCentered;
				break;
		}
	}
	public Image Image
	{
		get => GetNode<TextureRect>("%CardCreateImageBox").Texture?.GetImage();
		set => GetNode<TextureRect>("%CardCreateImageBox").Texture
			= ImageTexture.CreateFromImage(value);
	}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		(GetParent() as TabContainer)?.SetTabTitle(GetIndex(), Title);
		GetNode<BaseButton>("%StretchModeFitButton").ButtonGroup.Pressed
			+= StetchModeChanged;
		GetNode<LineEdit>("%TitleEdit").TextSubmitted +=
			(s) => GetNode<BaseButton>("%CreateMenuOkButton")
				.EmitSignal(Button.SignalName.Pressed);
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
	public void ClearMenu()
	{
		GetNode<TextureRect>("%CardCreateImageBox").Texture = new Texture2D();
		CardTitle = "";
		SetStretchMode(StretchMode.Unspecified);
	}
	public void SendCardToGame()
	{
		var c = card.MakeCard(GetTree());
		if (Image is Image im)
			c.SetImage(ImageTexture.CreateFromImage(im));
		c.SetStretchMode(GetStretchMode());
		c.CardName = CardTitle;
		this.GetParentOfType<game>().AddUnassignedCard(c);
	}
	public void FileSelected(string path)
	{
		Image image = new();
		image.Load(path);
		Image = image;
	}
}
