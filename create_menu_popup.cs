using Godot;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;

public partial class create_menu_popup : PanelContainer
{
	[Signal]
	public delegate void ClearMenuEventHandler();
	private CardCreateMarginContainer _CardContainer;
	public CardCreateMarginContainer CardContainer
	{ get
		{
			_CardContainer ??= GetNode<CardCreateMarginContainer>("%CardCreateMarginContainer");
			return _CardContainer;
		}
	}
	private RowCreateMarginContainer _RowContainer;
	public RowCreateMarginContainer RowContainer
	{ get
		{
			_RowContainer ??= GetNode<RowCreateMarginContainer>("%RowCreateMarginContainer");
			return _RowContainer;
		}
	}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}

    public void ShowPopup()
	{
		this.GetParentOfType<game>().MenuOpenDisableInteraction();
		Show();
	}
    public void ClosePopup()
	{
		Hide();
		EmitSignal(SignalName.ClearMenu);
		this.GetParentOfType<game>().MenuClosedEnableInteraction();
	}
	public void OkPressed()
	{
		var current = GetNode<TabContainer>("%CreateTabContainer").CurrentTab;
		if (current == 0)
		{
			CardContainer.SendCardToGame();
			ClosePopup();
		}
		else if (current == 1)
		{
			RowContainer.SendRowToGame();
			ClosePopup();
		}
		else
		{
			throw new Exception("No create container visible");
		}
	}
	public void CancelPressed()
	{
		ClosePopup();
	}
	private void OnVisibilityChange(bool visibility)
	{
		// EmitSignal(SignalName.Clear);
		var tabs = GetNode<TabContainer>("%CreateTabContainer");
		if (tabs.SelectNextAvailable())
			tabs.SelectPreviousAvailable();
		if (tabs.SelectPreviousAvailable())
			tabs.SelectNextAvailable();
	}
}
