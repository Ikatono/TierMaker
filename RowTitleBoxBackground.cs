using Godot;
using System;

public partial class RowTitleBoxBackground : Panel
{
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
	public override void _GuiInput(InputEvent @event)
    {
		if (@event.IsActionPressed("LocationMenu"))
		{
			RowMenu();
		}
    }
	private void RowMenu()
	{
		this.GetParentOfType<game>().EditRowInMenu(this.GetParentOfType<row>());
	}
	public override Variant _GetDragData(Vector2 atPosition)
	{
		var prev = MakePreview();
		var prev_root = new Control();
		prev_root.AddChild(prev);
		prev.Position = prev.Size / -2;
		SetDragPreview(prev_root);
		return this.GetParentOfType<row>();
	}
	private Panel MakePreview()
	{
		return Duplicate(0) as Panel;
	}
}
