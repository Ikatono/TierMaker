using Godot;
using System;

public partial class ChatMarginContainer : MarginContainer
{
	public readonly string Title = "Twitch";
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GetParentOrNull<TabContainer>().SetTabTitle(GetIndex(), Title);
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
}
