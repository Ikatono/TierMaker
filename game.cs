using Godot;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

public partial class game : Control
{
	private settings_popup _SettingsPopup;
	private settings_popup SettingsPopup
	{ get
		{
			_SettingsPopup ??= GetNode<settings_popup>("%SettingsPopup");
			return _SettingsPopup;
		}
	}
	private create_menu_popup _CreateMenuPopup;
	private create_menu_popup CreateMenuPopup
	{ get
		{
			_CreateMenuPopup ??= GetNode<create_menu_popup>("%CreateMenuPopup");
			return _CreateMenuPopup;
		}
	}
	[Export]
	private Vector2 _CardSize = new(200, 200);
	public Vector2 CardSize
	{
		get => _CardSize;
		set
		{
			_CardSize = value;
			if (IsNodeReady())
				PropogateCardSize();
		}
	}
	public void SetCardSize(Vector2 size)
	{
		CardSize = size;
	}
	public void PropogateCardSize()
	{
		foreach (var r in GetNode("%RowContainer").GetChildren().OfType<row>())
			r.CardSize = _CardSize;
		foreach (var c in GetNode("%UnassignedCardContainer").GetChildren().OfType<card>())
			c.CustomMinimumSize = _CardSize;
	}
	public bool AllowDragging = true;
	public IEnumerable<row> Rows
		=> GetNode("%RowContainer").GetChildren().OfType<row>();
	public IEnumerable<card> UnassignedCards
		=> GetNode("%UnassignedCardContainer").GetChildren().OfType<card>();
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		// var rows = this.GetAllDescendents<row>().ToArray();
		// for (int i = 0; i < 20; i++)
		// {
		// 	var c = card.MakeCard(GetTree());
		// 	c.CardName = $"Card {c.CardId}";
		// 	if (GD.RandRange(0, 1) == 1)
		// 	{
		// 		//add to a row
		// 		var r = rows[GD.RandRange(0, rows.Length - 1)];
		// 		r.AddCard(c);
		// 	}
		// 	else
		// 	{
		// 		AddUnassignedCard(c);
		// 	}
		// }

		
		PropogateCardSize();
	}
    public override void _UnhandledInput(InputEvent @event)
    {
        if (@event.IsActionPressed("OpenMenu"))
		{
			if (SettingsPopup.Visible)
			{
				SettingsPopup.ClosePopup();
			}
			else
			{
				CreateMenuPopup.ClosePopup();
				SettingsPopup.ShowPopup();
			}
			GetViewport().SetInputAsHandled();
		}
		else if (@event.IsActionPressed("CreationMenu"))
		{
			if (CreateMenuPopup.Visible)
			{
				CreateMenuPopup.ClosePopup();
			}
			else
			{
				SettingsPopup.ClosePopup();
				CreateMenuPopup.ShowPopup();
			}
			GetViewport().SetInputAsHandled();
		}
    }
	public void Clear()
	{
		var rowContainer = GetNode("%RowContainer");
		var unContainer = GetNode("%UnassignedCardContainer");
		foreach (var r in rowContainer.GetChildren())
		{
			rowContainer.RemoveChild(r);
			r.QueueFree();
		}
		foreach (var c in unContainer.GetChildren())
		{
			unContainer.RemoveChild(c);
			c.QueueFree();
		}
	}
    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(double delta)
	{
	}
	public row GetRowById(string id)
		=> GetTree().GetNodesInGroup("RowGroup").OfType<row>()
		.FirstOrDefault(r => r.RowId == id);
	public void AddRow(row row, int index = -1)
	{
		var node = GetNode<VBoxContainer>("%RowContainer");
		node.AddChild(row);
		node.MoveChild(row, index);
		PropogateCardSize();
    }
	public void RemoveRow(string id)
	{
		var r = GetRowById(id);
		GetNode<VBoxContainer>("%RowContainer").RemoveChild(r);
	}
	public void AddUnassignedCard(card c)
	{
		c.CustomMinimumSize = _CardSize;
		var node = GetNode<HFlowContainer>("%UnassignedCardContainer");
		node.AddChild(c);
	}
	public card RemoveUnassignedCard(string id)
	{
		var node = GetNode<HFlowContainer>("%UnassignedCardContainer");
		var c = node.GetAllDescendents<card>().FirstOrDefault(x => x.CardId == id);
		if (c is not null)
		{
			node.RemoveChild(c);
		}
		return c;
	}
	public void EditCardInMenu(card c)
	{
		GetNode<card_edit_popup>("%CardEditPopup").ActivateForCard(c);
	}
	public void EditRowInMenu(row r)
	{
		GetNode<row_edit_popup>("%RowEditPopup").OpenWithRow(r);
	}
	public card ClaimCard(string id)
	{
		foreach (row r in Rows)
		{
			if (r.TryRemoveCard(id) is card c)
				return c;
		}
		return RemoveUnassignedCard(id);
	}
	public card FindCard(string id)
	{
		if (string.IsNullOrWhiteSpace(id))
			return null;
		foreach (var c in this.GetAllDescendents<card>())
			if (c.CardId == id)
				return c;
		return null;
	}
	public row FindRow(string id)
	{
		foreach (var r in this.GetAllDescendents<row>())
			if (r.RowId == id)
				return r;
		return null;
	}

	public void MenuOpenDisableInteraction()
	{
		GetNode<Control>("%UiMask").MouseFilter = MouseFilterEnum.Stop;
	}
	public void MenuClosedEnableInteraction()
	{
		GetNode<Control>("%UiMask").MouseFilter = MouseFilterEnum.Ignore;
	}
	public void ImportGame(string filename)
	{
		using var reader = new StreamReader(
			new DeflateStream(
				new FileStream(filename, FileMode.Open),
				CompressionMode.Decompress
			)	
		);
		var s = reader.ReadToEnd();
		Serializer.LoadFromSerial(this, s);
	}
	public void ExportGame(string filename, ExportSettings es)
	{
		using var writer = new StreamWriter(
			new DeflateStream(
				new FileStream(filename, FileMode.OpenOrCreate),
				CompressionLevel.Optimal
			)
		);
		var json = Serializer.CreateGameJson(this, es);
		writer.Write(json);
	}
	#region Commands
	public void MoveCard(string cardId, string targetRowId, int? toIndex = null)
	{
		row r;
		if (targetRowId == "_")
			r = null;
		else
		{
			r = FindRow(targetRowId);
			if (r is null)
				throw new Exception($"row {r.RowId} not found");
		}
		var c = ClaimCard(cardId);
		if (c is null)
			throw new Exception($"card {c.CardId} not found");
		if (r is not null)
			r.AddCard(c, toIndex);
		else
			AddUnassignedCard(c);
	}
	public void MoveRow(string rowId, int toIndex)
	{
		var r = FindRow(rowId);
		if (r is null)
			throw new Exception($"row {r.RowId} not found");
		//TODO what if out of range?
		r.GetParent().MoveChild(r, toIndex);
	}
	public void DeleteCards(params string[] cardId)
	{
		//don't claim any cards unless all of them are found
		if (!cardId.Select(FindCard).All(x => x is not null))
			throw new Exception("not all cards found");
		foreach (var c in cardId.Select(ClaimCard))
		{
			c.QueueFree();
		}
	}
	public void DeleteRow(string rowId, bool deleteCards = false)
	{
		var r = FindRow(rowId);
		if (r is null)
			throw new Exception($"row {r.RowId} not found");
		if (!deleteCards)
		{
			var unassigned = GetNode("%UnassignedCardContainer");
			foreach (var c in r.Cards.ToArray())
				c.Reparent(unassigned);
		}
		//GetNode("%RowContainer").RemoveChild(r);
		r.QueueFree();
	}
	public void CreateCard(string title = null, ImageWithMetadata image = null)
	{
		var c = card.MakeCard(GetTree());
		if (!string.IsNullOrWhiteSpace(title))
			c.CardName = title;
		if (image is not null)
		{
			c.SetTexture(ImageTexture.CreateFromImage(image.Image));
			c.SetStretchMode(image.StretchMode);
		}
		AddUnassignedCard(c);
	}
	public void CreateRow(Color? color = null, string title = null)
	{
		var r = row.MakeRow(GetTree());
		if (!string.IsNullOrWhiteSpace(title))
			r.RowText = title;
		if (color is Color color1)
			r.RowColor = color1;
		AddRow(r);
	}
	public void RenameCard(string cardId, string newName)
	{
		FindCard(cardId).CardName = newName;
	}
	public void RenameRow(string rowId, string newTitle)
	{
		GetRowById(rowId).RowText = newTitle;
	}
	public void RecolorRow(string rowId, Color color)
	{
		GetRowById(rowId).RowColor = color;
	}
	public void ChangeCardImage(string cardId, ImageWithMetadata image)
	{
		var c = FindCard(cardId);
		if (c is null)
			throw new Exception($"card {c.CardId} not found");
        c.SetTexture(ImageTexture.CreateFromImage(image.Image));
		c.SetStretchMode(image.StretchMode);
	}
	#endregion //Commands
}
