using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using Godot;

public class SerialRow
{
    [JsonInclude]
    public string Text { get; set; }
    [JsonInclude]
    public SerialColor Color { get; set; }
    [JsonInclude]
    public SerialCard[] Cards { get; set; }
    public row ToRow(SceneTree tree)
    {
        var r = row.MakeRow(tree);
        r.RowText = Text;
        r.RowColor = Color;
        foreach (var c in Cards)
            r.AddCard(c.ToCard(tree));
        return r;
    }
}

