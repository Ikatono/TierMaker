using System.Text.Json.Serialization;
using Godot;

public class SerialImage
{
    [JsonInclude]
    public byte[] DataWebp { get; set; }
    [JsonInclude]
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public StretchMode StretchMode { get; set; }
    public ImageWithMetadata ToImageWithMetadata()
    {
        var im = new Image();
        im.LoadWebpFromBuffer(DataWebp);
        return new(im, StretchMode);
    }
}
