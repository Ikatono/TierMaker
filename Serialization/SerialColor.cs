using Godot;
using System.Text.Json.Serialization;

public class SerialColor
{
    [JsonInclude]
    public float R { get; set; }
    [JsonInclude]
    public float G { get; set; }
    [JsonInclude]
    public float B { get; set; }
    public void Deconstruct(out float r, out float g, out float b)
    {
        r = R;
        g = G;
        b = B;
    }
    public SerialColor()
    {
        R = 0;
        G = 0;
        B = 0;
    }
    public SerialColor(float r, float g, float b)
    {
        R = r;
        G = g;
        B = b;
    }
    public SerialColor(Color c)
    {
        R = c.R;
        G = c.G;
        B = c.B;
    }
    public static implicit operator Color(SerialColor sc)
        => new(
            sc.R,
            sc.G,
            sc.B
        );
}