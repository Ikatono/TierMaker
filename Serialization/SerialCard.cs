using Godot;
using System;
using System.Data.Common;
using System.Linq;
using System.Text.Json.Serialization;

public class SerialCard
{
    [JsonInclude]
    public string Text { get; set; }
    [JsonInclude]
    public SerialImage Image { get; set; }
    public card ToCard(SceneTree tree)
    {
        var c = card.MakeCard(tree);
        c.CardName = Text;
        if (Image.DataWebp.Any())
        {
            var iwm = Image.ToImageWithMetadata();
            c.SetTexture(ImageTexture.CreateFromImage(iwm.Image));
            c.SetStretchMode(iwm.StretchMode);
        }
        else
        {
            c.SetStretchMode(Image.StretchMode);
        }
        return c;
    }
}
