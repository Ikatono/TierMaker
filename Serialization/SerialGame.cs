using System.Text.Json.Serialization;

public class SerialGame
{
    [JsonInclude]
    public SerialRow[] Rows { get; set; }
    [JsonInclude]
    public SerialCard[] UnassignedCards { get; set; }
}