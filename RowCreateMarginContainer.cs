using Godot;
using System;

public partial class RowCreateMarginContainer : MarginContainer
{
	[Export]
	public Color DefaultColor = new(0, 0, 0, 1);
	public readonly string Title = "Row";
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		(GetParent() as TabContainer).SetTabTitle(GetIndex(), Title);
		ClearMenu();
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
	public void SendRowToGame()
	{
		var r = row.MakeRow(GetTree());
		r.RowColor = GetNode<ColorPickerButton>("%RowCreateColorPickerButton").Color;
		r.RowText = GetNode<TextEdit>("%RowTextEdit").Text;
		this.GetParentOfType<game>().CallDeferred("AddRow", r, -1);
	}
	public void ClearMenu()
	{
		GetNode<ColorPickerButton>("%RowCreateColorPickerButton").Color = DefaultColor;
		GetNode<TextEdit>("%RowTextEdit").Text = "";
	}
}
