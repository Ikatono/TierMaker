using Godot;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

public partial class settings_popup : PanelContainer
{
	private defer_manager _Deferer = null;
	private defer_manager Deferer
	{ get
		{
			_Deferer ??= GetNode<defer_manager>("/root/DeferManager");
			return _Deferer;
		}
	}
	public string Channel => GetNode<LineEdit>("%ChannelNameEdit").Text;
	[Signal]
	public delegate void BeforeOkEventHandler();
	[Signal]
	public delegate void ImportSelectedEventHandler(string filename);
	[Signal]
	public delegate void ExportSelectedEventHandler(string filename, ExportSettings settings);
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		DisplayServer.WindowSetMinSize(
			DisplayServer.WindowGetMinSize().Union(Size));
		GetNode<TwitchChatWatcher>("/root/TwitchChatWatcher")
			.Connect(TwitchChatWatcher.SignalName.SocketConnected,
			new Callable(this, nameof(SocketConnected)),
			(uint)ConnectFlags.Deferred);
		GetNode<TwitchChatWatcher>("/root/TwitchChatWatcher")
			.Connect(TwitchChatWatcher.SignalName.SocketDisconnected,
			new Callable(this, nameof(SocketDisconnected)),
			(uint)ConnectFlags.Deferred);
	}
	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		// //perform 1 action every frame
		// if (ActionQueue.TryDequeue(out Action a))
		// {
		// 	GD.Print("Found action");
		// 	a?.Invoke();
		// }
	}
	public void ShowPopup()
	{
		this.GetParentOfType<game>().MenuOpenDisableInteraction();
		Show();
	}
    public void ClosePopup()
	{
		Hide();
		this.GetParentOfType<game>().MenuClosedEnableInteraction();
	}
	public void _on_cancel_button_pressed()
	{
		ClosePopup();
	}
	public void _on_ok_button_pressed()
	{
		var settings = GetNode<Settings>("/root/Settings");
		settings.AllowModerators = GetNode<CheckBox>("%CheckBoxModerator").ButtonPressed;
		settings.Trigger = GetNode<LineEdit>("%CommandEdit").Text;
		settings.SetUserLists(GetNode<TextEdit>("%WhiteListEdit").Text.Split('\n',
			StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries),
			GetNode<TextEdit>("%BlackListEdit").Text.Split('\n',
			StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries));
		GetNode<ConfigStretchContainer>("%ConfigStretchContainer").Apply();
		ClosePopup();
	}
	public void _on_connect_button_pressed()
	{
		var tcw = GetNode<TwitchChatWatcher>("/root/TwitchChatWatcher");
		//string chName = GetNode<LineEdit>("%ChannelNameEdit").Text;
		Task.Run(tcw.ConnectAsync).ContinueWith(t => tcw.Authenticate(null, null))
			.ContinueWith(t => tcw.RequestTags())
			//.ContinueWith(t => tcw.JoinChannel(chName))
			.ContinueWith(FinishConnection);
		ProcessMode = ProcessModeEnum.Disabled;
	}
	public void _on_join_channel_button_pressed()
	{
		var channel = Channel;
		if (string.IsNullOrWhiteSpace(channel))
			return;
		var tcw = GetNode<TwitchChatWatcher>("/root/TwitchChatWatcher");
		_ = Task.Run(() => tcw.JoinChannel(channel));
	}
	private void FinishConnection(Task t)
	{
		if (t.IsCompletedSuccessfully)
		{
			CallDeferred(nameof(SuccessfulConnection));
		}
		else
		{
			GD.PrintErr(t.Exception);
			CallDeferred(nameof(UnlockPopup));
		}
	}
	private void UnlockPopup()
	{
		ProcessMode = ProcessModeEnum.Inherit;
	}
	private void SuccessfulConnection()
	{
		GD.Print(nameof(SuccessfulConnection));
		var tcw = GetNode<TwitchChatWatcher>("/root/TwitchChatWatcher");
		if (tcw.State != System.Net.WebSockets.WebSocketState.Open)
			throw new Exception("Websocket closed");
		GetNode<BaseButton>("%JoinChannelButton").Disabled = false;
		UnlockPopup();
	}
	private void SocketConnected()
	{
		GetNode<BaseButton>("%ConnectButton").Disabled = true;
	}
	private void SocketDisconnected()
	{
		GetNode<BaseButton>("%ConnectButton").Disabled = false;
	}
	private void ImportFileSelected(string filename)
	{
		EmitSignal(SignalName.ImportSelected, filename);
	}
	private void ExportFileSelected(string filename)
	{
		ExportSettings es = new()
		{
			ScaleImages = GetNode<BaseButton>("%ExportCompressPicturesButton").ButtonPressed,
		};
		EmitSignal(SignalName.ExportSelected, filename, Variant.From(es));
	}
}
