# Tier Maker

A basic tierlist maker that also connects to Twitch chat so your moderators can control it for you.

## How to

Drag cards to move them and rearrange them. Drag rows by their title cards (box on the left) to rearrange them.

Right click on a card to open the card edit/delete menu. You can change the text, image, and stretch mode here, or delete the card. Right click on a row title card to open the row edit/delete menu. You can edit the text and color here, or delete the row. For safety, delete buttons are locked by default.

Drag images onto the window to import them as a new card.

### Main menu

F1 opens the main menu. The first tab is for Twitch chat integration. Set channel, click "connect", then click "join". You can change the channel later with "join". Set the trigger command (documentation uses !tier) and grant user permissions here.

The second tab is for configuration. There's very little here right now.

The third tab is for import/export. Tier Maker exports to .tier files (deflate-compressed json) with card images embedded. These can be ver big if you unset the "scale images" button.

### Create menu

F2 opens the create menu. The first tab lets you create a new card, the second a new row. These are very similar to the edit menus.

NOTE: due to a bug (possibly in Godot itself), when you open the create menu it may not show the whole menu. Changing tabs should fix this.

### Twitch connection

After connected to Twitch chat, authorized users can control the tier list with the following commands. These commands reference cards and rows by their IDs, the number in the bottom left. When moving a row or placing a card within a row, *index* refers to how many cards/rows are before it, meaning *0* is the first spot.

Commands are written as:

*trigger* *commandname* arg1 arg2...

#### Command names:

    movecard CardID RowID [index]

Move card *CardID* to the row *RowID*. Placed at the end of the row unless index is provided. Use _ for *RowID* to move the card to the unassigned region at the bottom.

    moverow RowID index

Move row *RowID* to the given index.

    deletecard CardID [CardID2...]

Deletes one or more cards with the given IDs.

    deleterow RowId [DeleteCards?]

Deletes a single row with the given ID. By default, cards in this row are moved to the unassigned region, but if you end the command with "true" (!tier deleterow 4 true) then the cards will be deleted instead.

    createcard ImageUrl "Card text with spaces"

Creates a new card. Attempts to download the image from the provided URL (or use _ if there is no image). Everything after the URL is the card text, and spaces are kept.

    createrow Color "Row text with spaces"

Creates a new row. Accepts either hex color codes or some color names. Everything after the color is the row text, and spaces are kept.

    renamecard CardID "New card text with spaces"

Changes the text on an existing card.

    renamerow RowID "New row text with spaces"

Changes the text on an existing row.

    recolorrow RowID Color

Changes the color of an existing row.

    changecardimage CardID ImageUrl

Changes the image for an existing card.