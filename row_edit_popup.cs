using Godot;
using System;

public partial class row_edit_popup : PanelContainer
{
	[Export]
	private Color DefaultColor = new(0, 0, 0, 1);
	[Signal]
	public delegate void NowVisibleEventHandler();
	[Signal]
	public delegate void NowInvisibleEventHandler();
	public row EditingRow;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
	public void OpenWithRow(row r)
	{
		EditingRow = r;
		GetNode<ColorPickerButton>("%ColorPickerButton").Color = r.RowColor;
		GetNode<TextEdit>("%RowTextEdit").Text = r.RowText;
		this.GetParentOfType<game>().MenuOpenDisableInteraction();
		Show();
	}
	public void OkClicked()
	{
		EditingRow.RowColor = GetNode<ColorPickerButton>("%ColorPickerButton").Color;
		EditingRow.RowText = GetNode<TextEdit>("%RowTextEdit").Text;
		Reset();
	}
	public void CancelClicked()
	{
		Reset();
	}
	public void Reset()
	{
		Hide();
		GetNode<ColorPickerButton>("%ColorPickerButton").Color = DefaultColor;
		GetNode<TextEdit>("%RowTextEdit").Text = "";
		EditingRow = null;
		this.GetParentOfType<game>().MenuClosedEnableInteraction();
	}
	public void DeleteRow()
	{
		EditingRow.QueueFree();
		Reset();
	}
}
