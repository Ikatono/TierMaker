using Godot;

public class InputSingleton
{
    private static InputSingleton _SingletonCache;
    public static InputSingleton Instance
    {
        get
        {
            _SingletonCache ??= new();
            return _SingletonCache;
        }
    }
    private InputSingleton()
    {

    }
    public Node ClickedOn { get; set; }
    private void HandleMouseRelease()
    {
        
    }
}