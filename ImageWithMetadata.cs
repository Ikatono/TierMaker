using System.Data.Common;
using Godot;

public record class ImageWithMetadata
{
    public Image Image;
    public StretchMode StretchMode;
    public ImageWithMetadata(Image image)
    {
        Image = image;
        StretchMode = StretchMode.Unspecified;
    }
    public ImageWithMetadata(Image image, StretchMode stretchMode)
        : this(image)
    {
        StretchMode = stretchMode;
    }
    // public static implicit operator Image(ImageWithMetadata iwm)
    //     => iwm.Image;
    // public static implicit operator ImageWithMetadata(Image image)
    //     => new(image);
}

public enum StretchMode
{
    Unspecified,
    /// <summary>
    /// Fit to either the width or height of the card
    /// </summary>
    Fit,
    /// <summary>
    /// Stretch the image to fill the card
    /// </summary>
    Stretch,
    /// <summary>
    /// Crop image to fit card (maintain center)
    /// </summary>
    Crop,
}