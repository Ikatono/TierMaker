using Godot;
using System;

public partial class UnassignedCardPanel : PanelContainer
{
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
	public override bool _CanDropData(Vector2 atPosition, Variant data)
    {
        return data.As<card>() is not null;
    }
    public override void _DropData(Vector2 atPosition, Variant data)
    {
		card c = data.As<card>()
			?? throw new Exception("invalid drag data");
		var g = this.GetParentOfType<game>()
			?? throw new Exception("game instance not found");
		if (g.ClaimCard(c.CardId) is card _c)
		{
			if (!ReferenceEquals(c, _c))
				throw new Exception($"card move mismatch");
			GetNode<HFlowContainer>("%UnassignedCardContainer").AddChild(c);
		}
    }
}
