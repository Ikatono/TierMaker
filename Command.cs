using Godot;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

public partial class Command : GodotObject
{
    public string User;
    public bool IsStreamer;
    public bool IsModerator;
    // public CommandType Type;
    private string _Args;
    public CommandArguments GetArgs()
        => new(_Args);
    public Command(string user, bool isStreamer, bool isModerator,
        // CommandType type,
        string args)
    {
        User = user;
        IsStreamer = isStreamer;
        IsModerator = isModerator;
        // Type = type;
        _Args = args;
    }
}

/// <summary>
/// Manages a list of whitespace-delimited arguments. Does not
/// respect quotes or escape characters. A single argument containing
/// whitespace can be placed at the end and retrieved with <see cref="Remaining"/> 
/// after the previous arguments are popped.
/// </summary>
/// <remarks>
/// Use <see cref="DuplicateAtState"/> to safely handle subcommands.
/// </remarks>
public class CommandArguments
{
    private static readonly Regex _Regex = new(@"\s+");
    private readonly string OriginalArgs;
    private string Args;
    public CommandArguments(string args)
    {
        OriginalArgs = args;
        Args = args;
    }
    public string Pop()
    {
        var spl = _Regex.Split(Args, 2);
        Args = spl.ElementAtOrDefault(1) ?? "";
        return spl[0];
    }
    public string Remaining()
        => Args;
    public void Reset()
    {
        Args = OriginalArgs;
    }
    /// <summary>
    /// Enumerates arguments from current state, without changing state
    /// </summary>
    /// <returns></returns>
    public IEnumerable<string> Enumerate()
    {
        var a = DuplicateAtState();
        while (true)
        {
            var s = a.Pop();
            if (string.IsNullOrWhiteSpace(s))
                break;
            yield return s;
        }
    }
    /// <summary>
    /// Creates a new arguments object that returns to this state when reset.
    /// Any arguments that have already been popped will not return.
    /// </summary>
    public CommandArguments DuplicateAtState()
    {
        return new(Args);
    }
}

public enum CommandType
{
    MoveCard,
    MoveRow,
    DeleteCard,
    DeleteRow,
    CreateCard,
    CreateRow,
    RenameCard,
    RenameRow,
    RecolorRow,
    ChangeCardImage,
}

public static class CommandTypeHelper
{
    public static CommandType? ParseCommand(string commandType)
    {
        // if (Enum.TryParse(typeof(CommandType), commandType, true, out object ct))
        //     return (CommandType)ct;
        // return null;
        var c = commandType.ToLower();
        return c switch
        {
            "movecard" => CommandType.MoveCard,
            "moverow" => CommandType.MoveRow,
            "deletecard" or "deletecards" => CommandType.DeleteCard,
            "deleterow" => CommandType.DeleteRow,
            "createcard" or "addcard" => CommandType.CreateCard,
            "createrow" or "addrow" => CommandType.CreateRow,
            "renamecard" => CommandType.RenameCard,
            "renamerow" => CommandType.RenameRow,
            "recolorrow" or "changerowcolor" => CommandType.RecolorRow,
            "changecardimage" or "changecardpicture" => CommandType.ChangeCardImage,
            _ => null,
        };
    }
}