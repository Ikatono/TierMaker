using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Godot;
using Microsoft.VisualBasic;

public partial class card : Panel
{
	private Settings Settings => GetNode<Settings>("/root/Settings");
	[Export]
	private string _CardName;
	public string CardName
	{
		get => _CardName;
		set
		{
			_CardName = value;
			if (IsNodeReady())
				PropogateCardName();
		}
	}
	private void PropogateCardName()
	{
		GetNode<Label>("%CardNameLabel").Text = _CardName;
	}
	[Export]
	private string _CardId;
	public string CardId
	{
		get => _CardId;
		set
		{
			_CardId = value;
			if (IsNodeReady())
				PropogateCardId();
		}
	}
	private void PropogateCardId()
	{
		GetNode<Label>("%CardIdLabel").Text = _CardId;
	}
	private Texture2D _Texture;
	public void SetTexture(Texture2D texture)
	{
		_Texture = texture;
		if (IsNodeReady())
			PropogateTexture();
	}
	private void PropogateTexture()
	{
		if (_Texture is not null)
		{
			GetNode<TextureRect>("%CardImage").Texture = _Texture;
			_Texture = null;
		}
	}
	private StretchMode? _StretchMode = null;
	public void SetStretchMode(StretchMode stretchMode)
	{
		_StretchMode = stretchMode;
		if (IsNodeReady())
			PropogateStretchMode();
	}
	public StretchMode GetStretchMode()
	{
		var n = GetNode<TextureRect>("%CardImage");
		return (n.StretchMode, n.ExpandMode) switch
		{
			(TextureRect.StretchModeEnum.Scale,
				TextureRect.ExpandModeEnum.IgnoreSize)
				=> StretchMode.Stretch,
			(TextureRect.StretchModeEnum.KeepAspectCovered,
				TextureRect.ExpandModeEnum.IgnoreSize)
				=> StretchMode.Crop,
			(TextureRect.StretchModeEnum.KeepAspectCentered,
				TextureRect.ExpandModeEnum.IgnoreSize)
				=> StretchMode.Fit,
			_ => StretchMode.Unspecified,
		};
	}
	private void PropogateStretchMode()
	{
		var ci = GetNode<TextureRect>("%CardImage");
		switch (_StretchMode)
		{
			case null:
				break;
			case StretchMode.Stretch:
				ci.ExpandMode = TextureRect.ExpandModeEnum.IgnoreSize;
				ci.StretchMode = TextureRect.StretchModeEnum.Scale;
				break;
			case StretchMode.Crop:
				ci.ExpandMode = TextureRect.ExpandModeEnum.IgnoreSize;
				ci.StretchMode = TextureRect.StretchModeEnum.KeepAspectCovered;
				break;
			case StretchMode.Unspecified:
			case StretchMode.Fit:
			default:
				ci.ExpandMode = TextureRect.ExpandModeEnum.IgnoreSize;
				ci.StretchMode = TextureRect.StretchModeEnum.KeepAspectCentered;
				break;
		}
		_StretchMode = null;
	}

	public Texture2D GetTexture()
		=> GetNode<TextureRect>("%CardImage").Texture;
	public Vector2 Center
		=> Size / 2;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		PropogateCardName();
		PropogateCardId();
		PropogateTexture();
		PropogateStretchMode();
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{

	}
    public override Variant _GetDragData(Vector2 atPosition)
    {
        var prev = card_preview.MakePreview(this);
		var prev_root = new Control();
		prev_root.AddChild(prev);
		prev.Position = prev.Size / -2;
		SetDragPreview(prev_root);
		return this;
    }
    public void SetImage(Texture2D texture)
	{
		Ready += () => InnerSetImage(texture);
		if (IsNodeReady())
			InnerSetImage(texture);
	}
	//only called while ready
	private void InnerSetImage(Texture2D texture)
	{
		var node = GetNode<TextureRect>("%CardImage");
		node.Texture = texture;
	}
    public override void _GuiInput(InputEvent @event)
    {
		if (@event.IsActionPressed("LocationMenu"))
		{
			CardMenu();
		}
    }
	private void CardMenu()
	{
		this.GetParentOfType<game>().EditCardInMenu(this);
	}
	//TODO need to completely rework this
    public static card MakeCard(SceneTree tree)
	{
		var scene = GD.Load<PackedScene>("res://card.tscn");
		var c = scene.Instantiate<card>();
		string[] ids = {};
		if (Context is CardMakerContext cme)
			ids = cme.Ids.ToArray();
		c.CardId = tree.GetUnusedCardId(ids);
		if (Context is CardMakerContext cme2)
			cme2.AddNewId(c.CardId);
		return c;
	}
	public class CardMakerContext : IDisposable
	{
		private readonly SceneTree Tree;
		private readonly List<string> NewCards = new();
		public IEnumerable<string> Ids => NewCards;
		public void AddNewId(string id) => NewCards.Add(id);
		public CardMakerContext(SceneTree tree)
		{
			Tree = tree;
		}
		public void Dispose()
		{
			card.Context = null;
		}
	}
	private static CardMakerContext Context = null;
	public static CardMakerContext MakeContext(SceneTree tree)
	{
		if (Context is not null)
			return null;
		Context = new(tree);
		return Context;
	}
}
