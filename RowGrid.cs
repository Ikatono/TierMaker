using Godot;
using System;

public partial class RowGrid : PanelContainer
{
	protected Vector2 _CardSize;
	public Vector2 CardSize
	{
		get => CardSize;
		set
		{
			_CardSize = value;
			UpdateCardSize();
		}
	}
	protected Color _RowColor;
	public Color RowColor
	{
		get => _RowColor;
		set
		{
			_RowColor = value;
			UpdateRowColor();
		}
	}
	protected void UpdateCardSize()
	{
		foreach (var c in this.GetAllDescendents<card>())
			c.CustomMinimumSize = _CardSize;
	}
	public const float RowBackgroundColorScale = 2;
	private static Color ScaleColor(Color color)
	{
		return new Color
		{
			R = 1 - (1 - color.R) / RowBackgroundColorScale,
			G = 1 - (1 - color.G) / RowBackgroundColorScale,
			B = 1 - (1 - color.B) / RowBackgroundColorScale,
			A = color.A,
		};
	}
	protected void UpdateRowColor()
	{
		var sbf = GetThemeStylebox("panel").Duplicate() as StyleBoxFlat;
		sbf.BgColor = ScaleColor(_RowColor);
		AddThemeStyleboxOverride("panel", sbf);
		sbf = GetNode<Panel>("%RowTitleBoxBackground").GetThemeStylebox("panel").Duplicate() as StyleBoxFlat;
		sbf.BgColor = _RowColor;
		GetNode<Panel>("%RowTitleBoxBackground").AddThemeStyleboxOverride("panel", sbf);
	}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{

	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
}
