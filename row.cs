using Godot;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

public partial class row : Control
{
	private Settings Settings => GetNode<Settings>("/root/Settings");
	[Export]
	private Color _RowColor;
	public Color RowColor
	{
		get => _RowColor;
		set
		{
			_RowColor = value;
			if (IsNodeReady())
				PropogateColor();
		}
	}
	[Export]
	private Vector2 _CardSize;
	public Vector2 CardSize
	{
		get => _CardSize;
		set
		{
			_CardSize = value;
			if (IsNodeReady())
				PropogateCardSize();
		}
	}
	[Export]
	private string _RowText;
	public string RowText
	{
		get => _RowText;
		set
		{
			_RowText = value;
			if (IsNodeReady())
				PropogateRowText();
		}
	}
	private string _RowId;
	[Export]
	public string RowId
	{
		get => _RowId;
		set
		{
			_RowId = value;
			if (IsNodeReady())
				PropogateRowId();
		}
	}
	public IEnumerable<card> Cards
		=> GetNode("%RowCardContainer").GetChildren().OfType<card>();
	private void PropogateColor()
	{
		GetNode<RowGrid>("%RowGrid").RowColor = _RowColor;
	}
	private void PropogateCardSize()
	{
		//CustomMinimumSize = new Vector2(0, _CardSize.Y);
		GetNode<RowGrid>("%RowGrid").CardSize = _CardSize;
		GetNode<Panel>("%RowTitleBoxBackground").CustomMinimumSize = _CardSize;
	}
	private void PropogateRowText()
	{
		GetNode<Label>("%RowTitleBoxLabel").Text = _RowText;
	}
	private void PropogateRowId()
	{
		GetNode<Label>("%RowTitleBoxIdLabel").Text = _RowId;
	}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		//needs to wait until ready first
		PropogateColor();
		PropogateCardSize();
		PropogateRowText();
		PropogateRowId();
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
	public override void _Input(InputEvent @event)
    {
        
    }
	private void RowMenu()
	{

	}
    public void DropCardOn(Vector2 atPosition, Variant data)
    {
		card c = data.As<card>()
			?? throw new Exception("invalid drag data");
		var g = this.GetParentOfType<game>()
			?? throw new Exception("game instance not found");
		// if (!g.MoveCard(c.CardId, this, atPosition.X))
		// 	throw new Exception("failed to move card");
		if (g.ClaimCard(c.CardId) is card _c)
		{
			if (!ReferenceEquals(c, _c))
				throw new Exception("card move mismatch");
			//cards sorted with index
			foreach (var pair in Cards.Select((c, i) => (c.Position.X, i)))
			{
				if (atPosition.X >= (pair.X - c.Size.X / 2) && atPosition.X <= (pair.X + c.Size.X / 2))
				{
					GetNode("%RowCardContainer").AddChild(c);
					GetNode("%RowCardContainer").MoveChild(c, pair.i);
					return;
				}
			}
			GetNode("%RowCardContainer").AddChild(c);
			return;
		}
		else
		{
			throw new Exception($"Can't find card {c.CardId}");
		}
    }
	public override bool _CanDropData(Vector2 atPosition, Variant data)
    {
        return data.Obj is row;
    }
    public override void _DropData(Vector2 atPosition, Variant data)
    {
        var r = data.As<row>()
			?? throw new Exception("Invalid drop row");
		if (ReferenceEquals(this, r))
				return;
		var toIndex = GetIndex();
		if (atPosition.Y > Size.Y / 2)
			toIndex++;
		if (r.GetIndex() < GetIndex())
			toIndex--;
		GetParent().MoveChild(r, toIndex);
    }
    public void AddCard(card card, int? toIndex = null)
	{
		var n = GetNode("%RowCardContainer");
		n.AddChild(card);
		if (toIndex is int i)
		{
			n.MoveChild(card, i);
		}
	}
	public card GetCard(string id)
	{
		//inefficient to iterate through all children
		return GetNode("%RowCardContainer").GetChildren()
			.OfType<card>().FirstOrDefault(c => c.CardId == id);
	}
	public List<card> ClaimAllCards()
	{
		var cs = this.GetAllDescendents<card>().ToList();
		foreach (var c in cs)
		{
			RemoveChild(c);
		}
		return cs;
	}
	public card TryRemoveCard(string id)
	{
		var c = GetCard(id);
		if (c is not null)
		{
			GetNode("%RowCardContainer").RemoveChild(c);
		}
		return c;
	}
	public static row MakeRow(SceneTree tree)
	{
		var scene = GD.Load<PackedScene>("res://row.tscn");
		var r = scene.Instantiate<row>();
		r.RowId = tree.GetUnusedRowId();
		return r;
	}
}
