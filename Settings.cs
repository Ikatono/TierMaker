using System.Collections.Generic;
using System.Linq;
using Godot;

public partial class Settings : Node
{
    [Export]
    public bool AllowStreamer { get; set; }
    [Export]
    public bool AllowModerators { get; set; }
    [Export]
    public string Trigger { get; set; }
    public List<string> UserWhitelist { get; } = new();
    public List<string> UserBlacklist { get; } = new();
    public Vector2 CardSize { get; private set; }
    [Export]
    public StretchMode StretchMode { get; set; } = StretchMode.Fit;

    [Signal]
    public delegate void ChangeCardSizeEventHandler(Vector2 size);
    
    public void SetCardSize(Vector2 size)
    {
        CardSize = size;
        EmitSignal(SignalName.ChangeCardSize, size);
    }
    public bool IsUserAuthorized(string user, bool isStreamer = false, bool isModerator = false)
    {
        user = user.ToLower();
        if (UserBlacklist.Contains(user))
            return false;
        if (UserWhitelist.Contains(user))
            return true;
        if (AllowStreamer && isStreamer)
            return true;
        if (AllowModerators && isModerator)
            return true;
        return false;
    }
    public void SetUserLists(IEnumerable<string> white, IEnumerable<string> black)
    {
        UserWhitelist.Clear();
        UserWhitelist.AddRange(white.Select(s => s.ToLower()));
        UserBlacklist.Clear();
        UserBlacklist.AddRange(black.Select(s => s.ToLower()));
    }
}