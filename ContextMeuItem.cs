using Godot;
using System;

public partial class ContextMeuItem : Panel
{
	[Export]
	private string _ItemText;
	public string ItemText
	{
		get => _ItemText;
		set
		{
			_ItemText = value;
			if (IsNodeReady())
				PropogateText();
		}
	}
	private void PropogateText()
	{
		GetNode<Label>("%ContextMenuItemLabel").Text = _ItemText;
	}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{

		GuiInput += HandleClick;
		PropogateText();
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
	public void HandleClick(InputEvent @event)
	{
		// if (@event is InputEventMouseButton iemb)
		// {
		// 	if (iemb.ButtonIndex == MouseButton.Left && !iemb.Pressed)
		// }
	}
}
