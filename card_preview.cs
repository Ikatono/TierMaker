using Godot;
using System;

public partial class card_preview : PanelContainer
{
	[Export]
	private string _CardName;
	public string CardName
	{
		get => _CardName;
		set
		{
			_CardName = value;
			if (IsNodeReady())
				PropogateCardName();
		}
	}
	private void PropogateCardName()
	{
		GetNode<Label>("%CardPreviewNameLabel").Text = _CardName;
	}
	[Export]
	private string _CardId;
	public string CardId
	{
		get => _CardId;
		set
		{
			_CardId = value;
			if (IsNodeReady())
				PropogateCardId();
		}
	}
	private void PropogateCardId()
	{
		GetNode<Label>("%CardPreviewIdLabel").Text = _CardId;
	}
	private Texture2D _Texture;
	public void SetTexture(Texture2D texture)
	{
		_Texture = texture;
		// if (IsNodeReady())
			PropogateTexture();
	}
	private void PropogateTexture()
	{
		if (_Texture is not null)
		{
			GetNode<TextureRect>("%CardPreviewImage").Texture = _Texture;
			_Texture = null;
		}
	}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		PropogateCardId();
		PropogateCardName();
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
	public void SetImage(Texture2D texture)
	{
		Ready += () => InnerSetImage(texture);
		if (IsNodeReady())
			InnerSetImage(texture);
	}
	//only called while ready
	private void InnerSetImage(Texture2D texture)
	{
		var node = GetNode<TextureRect>("%CardPreviewImage");
		node.Texture = texture;
	}
	public static card_preview MakePreview(card c)
	{
		const float TRANSPARENCY = 0.75f;
		var scene = GD.Load<PackedScene>("res://card_preview.tscn");
		var prev = scene.Instantiate() as card_preview;
		prev.CardName = c.CardName;
		prev.CardId = c.CardId;
		prev.SetTexture(c.GetTexture());
		// prev.Size = c.Size;
		var tr = prev.GetNode<TextureRect>("%CardPreviewImage");
		tr.CustomMinimumSize = c.Size;
		var ci = c.GetNode<TextureRect>("%CardImage");
		tr.ExpandMode = ci.ExpandMode;
		tr.StretchMode = ci.StretchMode;
		prev.GetNode<Control>("%CardPreviewImage").CustomMinimumSize = c.Size;
		prev.Scale = c.Scale;
		prev.Modulate = new(1, 1, 1, TRANSPARENCY);
		return prev;
		// var preview = new Control();
		// preview.AddChild(prev);
		// prev.Position = prev.Size * -0.5f;
	}
}
