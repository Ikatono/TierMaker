using Godot;
using System;

public partial class card_edit_popup : ConfirmationDialog
{
	private LineEdit _TitleEdit;
	public LineEdit TitleEdit
	{ get
		{
			_TitleEdit ??= GetNode<LineEdit>("%TitleEdit");
			return _TitleEdit;
		}
	}
	private TextureRect _ImageBox;
	public TextureRect ImageBox
	{ get
		{
			_ImageBox ??= GetNode<TextureRect>("%CardEditImageBox");
			return _ImageBox;
		}
	}
	card EditingCard;
	[Export]
	private string Text
	{
		get => TitleEdit.Text;
		set => TitleEdit.Text = value;
	}
	[Export]
	private Texture2D Texture
	{
		get => ImageBox.Texture;
		set => ImageBox.Texture = value;
	}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GetNode<BaseButton>("%StretchModeFitButton").ButtonGroup.Pressed
			+= StetchModeChanged;
		TitleEdit.TextSubmitted +=
			(s) => GetOkButton().EmitSignal(Button.SignalName.Pressed);
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
	public void ActivateForCard(card c)
	{
		EditingCard = c;
		Text = c.CardName;
		Texture = c.GetTexture();
		SetStretchModeButton(c.GetStretchMode());
		Show();
	}
	public void OnVisibilityChange()
	{
		if (Visible)
		{
			TitleEdit.GrabFocus();
			TitleEdit.SelectAll();
		}
	}
	public void SetStretchModeButton(StretchMode stretchMode)
	{
		switch (stretchMode)
		{
			case StretchMode.Unspecified:
			case StretchMode.Fit:
			default:
				GetNode<BaseButton>("%StretchModeFitButton").ButtonPressed = true;
				break;
			case StretchMode.Stretch:
				GetNode<BaseButton>("%StretchModeStretchButton").ButtonPressed = true;
				break;
			case StretchMode.Crop:
				GetNode<BaseButton>("%StretchModeCropButton").ButtonPressed = true;
				break;
		}
	}
	public StretchMode GetStretchMode()
	{
		if (GetNode<BaseButton>("%StretchModeFitButton").ButtonPressed)
			return StretchMode.Fit;
		else if (GetNode<BaseButton>("%StretchModeStretchButton").ButtonPressed)
			return StretchMode.Stretch;
		else if (GetNode<BaseButton>("%StretchModeCropButton").ButtonPressed)
			return StretchMode.Crop;
		return StretchMode.Unspecified;
	}
	private void StetchModeChanged(BaseButton button)
	{
		var ci = ImageBox;
		switch (GetStretchMode())
		{
			case StretchMode.Stretch:
				ci.ExpandMode = TextureRect.ExpandModeEnum.IgnoreSize;
				ci.StretchMode = TextureRect.StretchModeEnum.Scale;
				break;
			case StretchMode.Crop:
				ci.ExpandMode = TextureRect.ExpandModeEnum.IgnoreSize;
				ci.StretchMode = TextureRect.StretchModeEnum.KeepAspectCovered;
				break;
			case StretchMode.Unspecified:
			case StretchMode.Fit:
			default:
				ci.ExpandMode = TextureRect.ExpandModeEnum.IgnoreSize;
				ci.StretchMode = TextureRect.StretchModeEnum.KeepAspectCentered;
				break;
		}
	}
	public void OkClicked()
	{
		EditingCard.CardName = Text;
		EditingCard.SetTexture(Texture);
		EditingCard.SetStretchMode(GetStretchMode());
		// Hide();
	}
	public void CancelClicked()
	{
		// Hide();
	}
	public void DeleteCard()
	{
		this.GetParentOfType<game>().DeleteCards(EditingCard.CardId);
		Hide();
	}
	public void FileSelected(string path)
	{
		Image image = new();
		image.Load(path);
		var texture = ImageTexture.CreateFromImage(image);
		Texture = texture;
	}
}
