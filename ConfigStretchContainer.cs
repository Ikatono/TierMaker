using Godot;
using System;

public partial class ConfigStretchContainer : VBoxContainer
{
	private Settings _Settings;
	private Settings Settings
	{ get
		{
			_Settings ??= GetNode<Settings>("/root/Settings");
			return _Settings;
		}
	}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
	public void LoadButtonState()
	{
		switch (Settings.StretchMode)
		{
			case StretchMode.Fit:
				GetNode<BaseButton>("%ConfigStretchFitButton").ButtonPressed = true;
				break;
			case StretchMode.Crop:
				GetNode<BaseButton>("%ConfigStretchCropButton").ButtonPressed = true;
				break;
			case StretchMode.Stretch:
				GetNode<BaseButton>("%ConfigStretchStretchButton").ButtonPressed = true;
				break;
			default:
				throw new Exception($"Unrecognized {nameof(StretchMode)} {(int)Settings.StretchMode}");
		}
	}
	public StretchMode GetStretchMode()
	{
		if (GetNode<BaseButton>("%ConfigStretchFitButton").ButtonPressed)
			return StretchMode.Fit;
		else if (GetNode<BaseButton>("%ConfigStretchCropButton").ButtonPressed)
			return StretchMode.Crop;
		else if (GetNode<BaseButton>("%ConfigStretchStretchButton").ButtonPressed)
			return StretchMode.Stretch;
		throw new Exception($"No {nameof(StretchMode)} buttons pressed");
	}
	public void Apply()
	{
		Settings.StretchMode = GetStretchMode();
	}
}
